export default [
    {
      path: "/login",
      name: "auth-login",
      component: () => import("@/views/pages/authentication/Login.vue"),
   
    },
  
  
  
    {
      path: "/verify",
      name: "auth-verify",
      component: () => import("@/views/pages/authentication/Verify.vue"),
   
    },
    {
        path: "/password",
        name: "password-setting",
        component: () => import("@/views/pages/authentication/PasswordSetting.vue"),
     
      },

  ];