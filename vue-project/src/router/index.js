import Vue from "vue";
import VueRouter from "vue-router";
import auth from "./routes/auth.js";
import roadmap from "./routes/roadmap.js";


Vue.use(VueRouter);

const router = new VueRouter({
  mode: "history",
  base: "https://4d105af1-ad0b-4759-96f8-eabf65bffd23.mock.pstmn.io/api/",
  routes: [
    { path: "/" },
    ...auth,
    ...roadmap,
    
  ],
});
export default router;
